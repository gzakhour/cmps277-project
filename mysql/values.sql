USE SNOOKER;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

INSERT INTO `championship` (`ID`, `Name`, `PIC`) VALUES
(1, 'Wuxi Classic', 'http://www.worldsnooker.com/javaImages/1c/39/0,,13165~10565916,00.jpg'),
(2, 'World Championship', 'http://www.worldsnooker.com/javaImages/1c/39/0,,13165~10565916,00.jpg');

INSERT INTO `tournament` (`ID`, `PRIZE`, `CHAMPIONSHIP_ID`, `YEAR`, `VENURE`) VALUES
(1, 1285900, 2, '2014', 'Crucible'),
(2, 2030000, 2, '2012', 'Crucible'),
(3, 1285900, 2, '2013', 'Crucible');

INSERT INTO `referee` (`Id`, `Name`, `Year_Started`) VALUES
(1, 'Michaela Tabb', '2002'),
(2, 'Jack Brownesome', '2004'),
(3, 'Patrick Jane', '2006'),
(4, 'Micheal Jacksome', '2009'),
(5, 'Peter Tallman', '2001');

INSERT INTO `cue_stick` (`Model_No`, `Name`, `Weight`, `Wood`) VALUES
(1, 'Paris Cues', 496, 'Maple wood'),
(2, 'Robert Osborne', 512, 'Olive wood'),
(3, 'Black Shockwave', 556, 'Maple wood'),
(4, 'Viper Commercial 1-Piece', 448, 'Hardwood'),
(5, 'Valley House Bar', 501, 'Titanium');

INSERT INTO `player` (`ID`, `Name`, `DATEOFBIRTH`) VALUES
(1, 'Ronnie O''Sullivan', '1982-10-05'),
(2, 'Steve Davis', '1975-01-29'),
(3, 'Mark Selby', '1984-10-08'),
(4, 'Stephen Hendry', '1981-05-13'),
(5, 'Kinan Bab', '1994-01-13'),
(6, 'Ali Cherri', '1994-03-12'),
(7, 'George Zakhidiot', '1994-08-23');

INSERT INTO `player_cue_stick` (`CUE_STICK_Model_No`, `PLAYER_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(3, 3),
(4, 3),
(4, 4),
(5, 4),
(1, 5),
(2, 6),
(2, 7);

INSERT INTO `player_img` (`PLAYER_ID`, `IMG`) VALUES
(1, 'http://blogs.tribune.com.pk/wp-content/uploads/2010/09/ronnie-sullivan-640x480.jpg'),
(1, 'http://i.telegraph.co.uk/multimedia/archive/02390/Ronnie_OSullivan_2390071b.jpg'),
(1, 'http://images.china.cn/attachement/jpg/site1007/20080506/000802c98ccc098a2e2b04.jpg'),
(1, 'http://news.bbcimg.co.uk/media/images/62533000/jpg/_62533796_sullivan.jpg'),
(2, 'http://upload.wikimedia.org/wikipedia/commons/c/cc/Steve_Davis_at_Sports_Club_Turku,_Finland.jpg'),
(2, 'https://mully1.files.wordpress.com/2010/04/steve-davis.jpg'),
(3, 'http://e1.365dm.com/13/10/660x350/Selby_3019981.jpg?20131016200542'),
(3, 'http://static.guim.co.uk/sys-images/Sport/Pix/pictures/2008/04/18/selbygetchnphots372.jpg'),
(4, 'http://i.telegraph.co.uk/multimedia/archive/01393/stephen-hendry_1393200c.jpg'),
(4, 'http://www.scottish-kilts.co.uk/wp-content/uploads/45100067_hendry512.jpg'),
(5, 'http://thumbs.dreamstime.com/x/unknown-businessman-avatar-profile-picture-black-white-illustration-35616527.jpg'),
(6, 'http://thumbs.dreamstime.com/x/unknown-businessman-avatar-profile-picture-black-white-illustration-35616527.jpg'),
(7, 'http://thumbs.dreamstime.com/x/unknown-businessman-avatar-profile-picture-black-white-illustration-35616527.jpg');

INSERT INTO `pro_player` (`PLAYER_ID`, `Pot_rate`, `NoCenturies`, `NoHalfCenturies`, `No147`) VALUES
(1, 84, 620, 840, 12),
(2, 76, 413, 223, 1),
(3, 83, 510, 723, 2),
(4, 70, 100, 152, 0);

INSERT INTO `match` (`ID`, `BESTOF`) VALUES
(1, 5),
(2, 5),
(3, 5),
(4, 5),
(5, 3),
(6, 3);

INSERT INTO `friendly_match` (`Match_id`, `Winner_id`, `Loser_id`) VALUES
(5, 6, 7),
(6, 5, 7);

INSERT INTO `tournament_match` (`Match_id`, `Referee_id`, `TOURNAMENT_id`, `Pro_winner_id`, `Pro_loser_id`) VALUES
(1, 5, 1, 3, 1),
(2, 1, 2, 1, 2),
(3, 2, 3, 1, 4),
(4, 5, 1, 4, 2);

INSERT INTO `frames` (`Frame_id`, `Match_id`, `Winner_id`, `Loser_id`, `TIME`, `Winner_score`, `Loser_score`) VALUES
(1, 1, 1, 3, 2000, 147, 0),
(2, 1, 3, 1, 2300, 67, 45),
(3, 1, 3, 1, 1100, 100, 12),
(4, 1, 1, 3, 4423, 61, 59),
(5, 1, 3, 1, 2048, 69, 36),
(6, 2, 1, 2, 750, 147, 0),
(7, 2, 1, 2, 1024, 130, 11),
(8, 2, 1, 2, 512, 102, 2),
(9, 3, 1, 4, 2002, 62, 32),
(10, 3, 1, 4, 2086, 59, 36),
(11, 3, 1, 4, 1777, 69, 13),
(12, 4, 4, 2, 10000, 62, 61),
(13, 4, 4, 2, 1000, 76, 23),
(14, 4, 4, 2, 2002, 75, 22),
(15, 5, 6, 7, 202, 147, 0),
(16, 5, 6, 7, 101, 147, 0),
(17, 6, 5, 7, 10, 146, 1),
(18, 6, 5, 7, 40, 147, 0);

INSERT INTO `completed` (`Pro_Id`, `TOURNAMENT_id`, `Rank`) VALUES
(1, 1, 2),
(1, 2, 1),
(1, 3, 1),
(2, 2, 2),
(3, 1, 1),
(4, 3, 2);