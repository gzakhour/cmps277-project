<?php
	$ID1 = $_REQUEST['player1'];
	$ID2 = $_REQUEST['player2'];
	$bestof = $_REQUEST['bestof'];
	$frames = explode("\n", $_REQUEST['frames']);
	
	$query = "INSERT INTO `match` values(NULL, $bestof, NOW());";
	$db = new PDO("mysql:dbname=snooker", "root", "");
	$db->exec($query);
	$matchID = $db->lastInsertId();
	
	$wins1 = 0;
	$wins2 = 0;
	
	foreach($frames as $frame) {
		$fr = explode(",", $frame);
		
		$score1 = $fr[0];
		$score2 = $fr[1];
		$tm = $fr[2];
		
		$winID = $ID1;
		$losID = $ID2;
		
		if($score2 > $score1) {
			$winID = $ID2;
			$losID = $ID1;
			
			$tmp = $score1;
			$score1 = $score2;
			$score2 = $tmp;
			
			$wins2 = $wins2 + 1;
		} else {
			$wins1 = $wins1 + 1;
		}
				
		$query = "INSERT INTO `frames` values(NULL, $matchID, $winID, $losID, $tm, $score1, $score2);";
		$db->exec($query);
	}
	
	if($wins2 > $wins1) {
		$tmp = $ID1;
		$ID1 = $ID2;
		$ID2 = $tmp;
	}
	
	$query = "INSERT INTO `friendly_match` values($matchID, $ID1, $ID2);";
	$db->exec($query);
	
	header("Location: ../#/add");
?>