<div class="row">
    <h4>Search results for <strong>{{query}}</strong></h4>
</div>

<div class="row" style="margin-top:20px">
    <div class="col-md-2">
        <ul class="nav nav-pills">
            <li{{#if is_all}} class="active"{{/if}}>
                <a href="#/search/{{query_encoded}}">All
                    <span class="badge pull-right">{{sum results.championships.length results.players.length}}</span>
                </a></li>
            <li{{#if is_players}} class="active"{{/if}}>
                <a href="#/search/{{query_encoded}}/players">Players <span class="badge pull-right">{{results.players.length}}</span></a></li>
            <li{{#if is_championships}} class="active"{{/if}}>
                <a href="#/search/{{query_encoded}}/championships">Championships <span class="badge pull-right">{{results.championships.length}}</span></a></li>
        </ul>
    </div>
    <div class="col-md-10">
        {{#with results}}
        <div class="row">
            {{#ifOR ../is_all ../is_players}}
            <div class="col-md-{{#if ../../is_all}}6{{else}}12{{/if}}">
            <h5>Players</h5>
            <table class="table table-responsive table-striped search_results">
                {{#each players}}
                <tr>
                    <td class="name"><a href="#/players/{{id}}">{{name}}</a></td>
                    <td><span class="label label-{{#if_eq type "Amateur"}}info{{else}}danger{{/if_eq}} pull-right">{{type}}</span></td>
                </tr>
                {{/each}}
            </table>
            </div>
            {{/ifOR}}
            {{#ifOR ../is_all ../is_championships}}
            <div class="col-md-{{#if ../../is_all}}6{{else}}12{{/if}}">
                <h5>Championships</h5>
                <table class="table table-responsive table-striped search_results">
                    {{#each championships}}
                        <tr>
                            <td class="name"><a href="#/championships/{{id}}">{{name}}</a></td>
                            <td>
                                {{#each tournaments}}
                                    <a href="#/tournaments/{{id}}">
                                        <span class="label label-info pull-right">{{year}}</span>
                                    </a>
                                {{/each}}
                            </td>
                        </tr>
                    {{/each}}
                </table>
            </div>
            {{/ifOR}}
        </div>
        {{/with}}
    </div>
</div>
