{{#with championship}}
<div class="row">
    <h2>{{name}}</h2>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Year</th>
                <th>Venue</th>
                <th>Winner</th>
                <th>Prize</th>
            </tr>
        </thead>
        <tbody>
            {{#each tournaments}}
                <tr>
                    <td><a href="#/tournaments/{{id}}">{{year}}</a></td>
                    <td>{{venue}}</td>
                    {{#with winner}}
                    <td><a href="#/players/{{id}}">{{name}}</a></td>
                    {{/with}}
                    <td>{{prize}}</td>
                </tr>
            {{/each}}
        </tbody>
    </table>
</div>
{{/with}}
