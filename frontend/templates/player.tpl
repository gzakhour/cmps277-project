<h3>{{name}}
    <span class="label label-{{#if_eq type "Professional"}}danger{{else}}info{{/if_eq}}" style="margin-left: 20px">{{type}}</span></h3>
<div class="row">
    <div class="col-sm-4">
        <img src="{{pics.[0]}}" class="img-responsive img-thumbnail" style="max-width:250px">
    </div>
    <div class="col-sm-8">
        <ul>
            <li><strong>Name:</strong> {{name}}</li>
            <li><strong>Date of Birth:</strong> {{dob}}</li>
            <li><strong>Cues Used:</strong> <ul>
                {{#each cues}}
                    <li>{{this}}</li>
                {{/each}}
            </ul></li>
            {{#if_eq type "Professional"}}
            <li><strong>Pot Rate:</strong> {{potrate}}</li>
            <li><strong>Number of centuries:</strong> {{centuries}}</li>
            <li><strong>Number of half-centuries:</strong> {{half-centuries}}</li>
            <li><strong>Number of 147s:</strong> {{no147}}</li>
            {{/if_eq}}
            <li><strong>Wins:</strong> {{wins}}</li>
            <li><strong>Loses:</strong> {{loses}}</li>
        </ul>
    </div>
</div>

<div class="row">
    <h3>Images of {{name}}</h3>
    {{#each pics}}
        <a href="{{this}}" target="_blank"><img src="{{this}}" class="img img-responsive img-thumbnail" style="max-width: 200px"></a>
    {{/each}}
</div>
