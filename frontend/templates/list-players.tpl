<div class="row">
    <h1>Listing all {{search_title}} Players</h1>
    <br>
</div>

<ul class="list-group" id="players-result">
{{#each players}}
    <li class="list-group-item col-lg-6">
        <div class="row">
            <div class="col-lg-2">
                <img src="{{pic}}" class="img-responsive img-circle" style="height:50px;">
            </div>
            <div class="col-lg-8">
                <h4><a href="#/players/{{id}}">{{name}}</a></h4>
            </div>
            <div class="col-lg-2">
                <span class="label pull-right label-{{#if_eq type "Amateur"}}info{{else}}danger{{/if_eq}}">
                    {{type}}
                </span>
            </div>
        </div>
    </li>
{{/each}}
</ul>

<div style="clear:both;"></div>

<ul class="pagination pagination-sm">
    {{#each pagecount}}
    <li{{#if_eq ../pagenum this}} class="active"{{/if_eq}}>
        <a href="#/players{{../search_type}}/page/{{this}}">{{this}}</a>
    </li>
    {{/each}}
</ul>
