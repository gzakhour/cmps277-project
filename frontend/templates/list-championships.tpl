<div class="row">
    <h1>Listing all Championships</h1>
    <br>
</div>

<ul class="list-group" id="players-result">
{{#each championships}}
    <li class="list-group-item col-lg-6">
        <div class="row">
            <div class="col-lg-2">
                <img src="{{pic}}" class="img-responsive img-circle" style="height:50px;">
            </div>
            <div class="col-lg-10">
                <h4><a href="#/championships/{{id}}">{{name}}</a></h4>
            </div>
        </div>
    </li>
{{/each}}
</ul>

<div style="clear:both;"></div>

<ul class="pagination pagination-sm">
    {{#each pagecount}}
    <li{{#if_eq ../pagenum this}} class="active"{{/if_eq}}>
        <a href="#/championships/page/{{this}}">{{this}}</a>
    </li>
    {{/each}}
</ul>
