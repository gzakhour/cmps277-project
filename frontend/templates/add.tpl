<div class="row" style="margin-bottom:20px">
    <h2>Delete matchs</h2>
    <form action="handlers/delete.php" method="post">
        <select multiple name="matchid" class="form-control">
            {{#each matchs}}
            <option value="{{id}}">{{name}}</option>
            {{/each}}
        </select><br>
        <input type="submit" value="Delete" class="btn btn-primary">
    </form>
</div>

<div class="row" style="margin-bottom:20px">
    <h2>Insert a new friendly match</h2>
    <form action="handlers/add.php" method="post">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="player1">Player 1</label>
                    <select id="p1" name="player1" class="form-control">
                        {{#each players}}
                        <option value="{{id}}">{{name}}</option>
                        {{/each}}
                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="player2">Player 2</label>
                    <select id="p2" name="player2" class="form-control">
                        {{#each players}}
                        <option value="{{id}}">{{name}}</option>
                        {{/each}}
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-1">Bestof:</div>
            <div class="col-xs-10">
                <input name="bestof" type="range" min="1" max="35" step="2" value="11">
            </div>
            <div class="col-xs-1">
                <span id="rangeval" class="pull-right">11</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            <h5>Insert Frame results</h5>
            <i>The format is as follows:</i><br>
            &lt;frame[0].player[0].score&gt;,&lt;frame[0].player[1].score&gt;,&lt;time in seconds&gt;<br>
            &lt;frame[1].player[0].score&gt;,&lt;frame[1].player[1].score&gt;,&lt;time in seconds&gt;<br>
            &lt;frame[2].player[0].score&gt;,&lt;frame[2].player[1].score&gt;,&lt;time in seconds&gt;<br>
            <textarea class="form-control" id="frames" name="frames"></textarea>
            </div>
        </div>
        <br>
        <input type="submit" value="Insert new match" class="btn btn-primary">
    </form>
</div>

<script>
$("input[type=range]").mouseup(function() {
    $("#rangeval").html($(this).val());
});
$("#load").click(function() {
    var v = $("#matchid").val();
    if(v != -1) {
        $.ajax({
            url: getjson("match.json?id="+v),
            success: function(data) {
                $("#p1").val(data.players[0].id);
                $("#p2").val(data.players[1].id);
                $("#rangeval").html(data.bestof);
                $("input[type=range]").val(data.bestof);
                var frames = "";
                for(frame in data.frames) {
                    frame = data.frames[frame];
                    frames += frame[0]+","+frame[1]+"\n";
                }
                $("#frames").val(frames);
            }
        });
    }
    return false;
});
</script>
