<h2 style="margin-bottom: 50px;">
    {{#if_eq type "Tournament"}}
        {{#with championship}}
        <a href="#/tournaments/{{id}}">{{name}}</a>
        {{/with}}
        <span class="label label-danger" style="margin-left:20px">{{type}}</span>
    {{else}}
        <span class="label label-info">{{type}}</span>
    {{/if_eq}}
</h2>
<div class="match">
<div class="row" id="match-thumbs">
    <div class="col-xs-6">
        <div class="pull-left">
            <a href="#/players/{{players.[0].id}}">
                <img src="{{players.[0].pic}}" class="img-responsive">
            </a>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="pull-right">
            <a href="#/players/{{players.[1].id}}">
                <img src="{{players.[1].pic}}" class="img-responsive">
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-5 player">
        {{players.[0].name}}
        {{#if_eq winner players.[0].name}}
            <span class="label label-success pull-right">Winner</span>
        {{/if_eq}}
    </div>

    <div class="col-xs-2 framescore">
        <div class="col-xs-12 time" style="margin-top:-{{#if has_referee}}68{{else}}52{{/if}}px">
            {{date}}<br>
            {{duration}}<br>
            {{#if has_referee}}
            Referee: {{referee}}
            {{/if}}
        </div>
        <span class="pull-left">{{score.[0]}}</span>
        ({{bestof}})
        <span class="pull-right">{{score.[1]}}</span>
    </div>

    <div class="col-xs-5 player">
        {{#if_eq winner players.[1].name}}
            <span class="label label-success pull-left">Winner</span>
        {{/if_eq}}
        <span class="pull-right">{{players.[1].name}}</span>
    </div>
</div>

<div id="frames">
{{#each frames}}
    <div class="row">
        <div class="col-xs-2 col-xs-offset-5 framescore">
            <span class="pull-left{{#if_eq winner ../players.[0].name}} winner{{/if_eq}}">
                {{this.[0]}}</span>
            <span class="pull-right{{#if_eq winner ../players.[1].name}} winner{{/if_eq}}">
                {{this.[1]}}</span>
        </div>
    </div>
{{/each}}
</div>
</div>
