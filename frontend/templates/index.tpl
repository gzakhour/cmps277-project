<div class="jumbotron">
    <h2 style="text-align:center">Search for a Player, Championship or Tournament</h2>
    <div class="form-group" style="margin: 50px 0;">
        <div class="input-group">
        <input type="text" class="form-control col-lg-8" id="search-home"
               placeholder="Ronnie O'Sullivan or UK Championship 2014 ...">
        <div class="input-group-btn">
            <button class="btn btn-primary" id="search-btn">Search</button>
        </div>
        </div>
    </div>
    <br>
</div>

<div class="row" id="homerow">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Latest Matchs</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    {{#each matchs}}
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-4">
                                {{#with player1}}
                                <a href="#/players/{{id}}">{{name}}</a>
                                {{/with}}
                            </div>
                            <div class="col-xs-1">vs</div>
                            <div class="col-xs-4">
                                {{#with player2}}
                                <a href="#/players/{{id}}">{{name}}</a>
                                {{/with}}
                            </div>
                            <div class="col-xs-3">
                                <a href="#/match/{{id}}"><span class="label 
                                {{#if_eq type "Tournament"}}
                                    label-danger
                                {{else}}
                                    label-primary
                                {{/if_eq}}
                                pull-right">{{type}}</span></a>
                            </div>
                        </div>
                    </li>
                    {{/each}}
                </ul>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Latest Tournaments</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    {{#each tournaments}}
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-8">
                                <a href="#/tournaments/{{id}}">{{name}}</a>
                            </div>
                            <div class="col-xs-4">
                                <span class="label label-info pull-right"> {{year}} </span>
                            </div>
                        </div>
                    </li>
                    {{/each}}
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
$("#search-home").keyup(function(e) {
    if(e.keyCode == 13)
        location.hash = "#/search/"+encodeURIComponent($(this).val());
});
$("#search-btn").click(function() {
    location.hash = "#/search/"+encodeURIComponent($("#search-home").val());
});
</script>
