{{#with tournament}}
    <div class="row" style="margin-bottom:20px">
        <h2><a href="#/championships/{{championship.id}}">{{championship.name}}</a>
            <span class="label label-info" style="margin-left:20px">{{year}}</span></h2>
    </div>

    {{#each matchs}}
    <div class="row match" style="margin:10px 0;cursor:pointer;" data-id="{{id}}">
        <div class="col-xs-5 player">
            <span class="pull-left">
                {{players.[0]}}
            </span>
        </div>
        <div class="col-xs-2 framescore">
            <span class="pull-left">{{score.[0]}}</span>
            ({{bestof}})
            <span class="pull-right">{{score.[1]}}</span>
        </div>
        <div class="col-xs-5 player">
            <span class="pull-right">
                {{players.[1]}}
            </span>
        </div>
    </div>
    {{/each}}
{{/with}}

<script>
$(".match").click(function() {
    window.location.hash = "#/match/"+($(this).data().id);
});
</script>
