function Router(callback404) {
    var __routes = [];
    return {
        add: function(pattern, callback) {
            __routes.push({
                "pattern": pattern,
                "callback": callback
            });
        },
        route: function() {
            $("body").scrollTop(0);
            var url = location.hash.slice(1) || "/";
            for(route in __routes) {
                var matches = url.match(__routes[route].pattern);
                if(matches !== null) {
                    __routes[route].callback.apply(null, matches);
                    return true;
                }
            }
            if(typeof callback404 == "function")
                callback404();
            return false;
        },
        init: function() {
            var that = this;
            window.addEventListener("hashchange",function(){that.route();});
            window.addEventListener("load",function(){that.route();});
        }
    }
}
