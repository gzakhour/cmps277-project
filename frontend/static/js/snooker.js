var pathToPhp = "./php/";
var pathToTemplate = "./templates";
function getPhpUrl(a) {
    return pathToPhp+a;
}

function init_load() {
    $("#content").html("<h3 style='margin-top:100px'>Loading...</h3>");
}

function load_and_compile(model, view, extras) {
    init_load();
    $.ajax({
        url: view,
        success: function(data) {
            var template = Handlebars.compile(data);
            $.ajax({
                url: getPhpUrl(model),
                success: function(data) {
					data = jQuery.parseJSON(data);
                    var content = data;
                    if(typeof extras !== "undefined") {
                        extra = extras(data);
                        for(m in extra)
                            content[m] = extra[m];
                    }
                    $("#content").html(template(content));
                },
                error: function() {
                    console.log("Could not connect to the .PHP Files", model);
                    console.log(arguments);
                }
            });
        },
        error: function() {
            console.log("Could not load the view", view);
            console.log(arguments);
        }
    });
}

function epoch_to_time(duration) {
    var duration_hours = Math.floor(duration/3600);
    var duration_min   = Math.floor((duration - duration_hours*3600)/60);
    var duration_sec   = duration % 60;
    duration_min = (duration_min < 10 ? "0"+duration_min : duration_min);
    duration_sec = (duration_sec < 10 ? "0"+duration_sec : duration_sec);
    return duration_hours+":"+duration_min+":"+duration_sec;
}

function date_to_str(epoch) {
    var d = new Date(0);
	d.setUTCSeconds(epoch);
	return d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear();
}


// Create the routing rules
var router = new Router(function() {
    $("#content").html("<div class='jumbotron'>\
    <h1>404 - Go <a href='#/'>home</a> you're drunk</h1>\
    <p>The page you're requesting is not available</p>\
    </div>");
});


// Add the homepage
router.add("^/$", function() {
    load_and_compile("latest.php",
                     pathToTemplate+"/index.tpl");
});


// Add the view to display the top users
router.add("^/players((/professional|/amateur|/top)?(/page/\\d+)?)$",
           function(url, whole, sub, pagenum) {
    var url = "list-players.php";
    var result_per_page = 10;
    if(typeof sub !== "undefined") url += "?q="+sub.slice(1);
    else sub = "";
    if(typeof pagenum === "undefined") pagenum = 1;
    else pagenum = parseInt(pagenum.slice(6));
    load_and_compile(url,
         pathToTemplate+"/list-players.tpl",
         function(data) {
            pagecount = Array.apply(null, {
                            length: Math.ceil(data.players.length/result_per_page)
                        }).map(Number.call, Number).map(function(x){
                            return x+1;
                        });
            players = data.players.slice((pagenum-1)*result_per_page, pagenum*result_per_page);
            return {
                "search_type": sub,
                "search_title": sub.charAt(1).toUpperCase()+sub.slice(2),
                "pagenum": pagenum,
                "pagecount": pagecount,
                "players": players
            }
         });
});


// Add the view of the championships
router.add("^/championships(/page/(\\d+))?$", function(url, sub, pagenum) {
    var result_per_page = 6;
    if(typeof pagenum === "undefined") pagenum = 1;
    else pagenum = parseInt(pagenum);
    load_and_compile("list-championships.php",
         pathToTemplate+"/list-championships.tpl",
         function(data) {
            pagecount = Array.apply(null, {
            length: Math.ceil(data.championships.length/result_per_page)
            }).map(Number.call, Number).map(function(x) {
                return x+1;
            });
            championships = data.championships.slice((pagenum-1)*result_per_page, pagenum*result_per_page);
            return {
                "players": data["championships"],
                "pagenum": pagenum,
                "pagecount": pagecount,
                "championships": championships
            };
         });
});


router.add("^/match/(\\d+)$", function(ignore, matchid) {
    matchid = parseInt(matchid);
    load_and_compile("match.php?id="+matchid,
         pathToTemplate+"/match.tpl",
         function(data) {
            scores = [0, 0];
            for(frame in data.frames) {
                frame = data.frames[frame];
                if(frame[0] > frame[1]) {
                    scores[0]++;
                    frame["winner"] = data.players[0].name;
                } else {
                    scores[1]++;
                    frame["winner"] = data.players[1].name;
                }
            }
            return {
                "score": scores,
                "winner": (scores[0] > scores[1] ? data.players[0].name :
                                                   data.players[1].name),
                "duration": epoch_to_time(data.duration),
                "date": date_to_str(data.date),
                "has_referee": (typeof data.referee !== "undefined")
            }
         });
});

router.add("^/tournaments/(\\d+)$", function(url, tid) {
    tid = parseInt(tid);
    load_and_compile("tournaments.php?id="+tid,
        pathToTemplate+"/tournaments.tpl");
});

router.add("^/championships/(\\d+)$", function(url, cid) {
    cid = parseInt(cid);
    load_and_compile("championships.php?id="+cid,
        pathToTemplate+"/championships.tpl",
        function(data) {
            data.championship.tournaments.sort(function(a, b) {
                return b.year - a.year;
            });
            return data;
        });
});

router.add("^/search/([^/]+)(/\\w+)?$", function(url, search, type) {
    var query = decodeURIComponent(search);
    load_and_compile("search.php?q="+search,
        pathToTemplate+"/search.tpl",
        function(data) {
            return {
                "query": query,
                "query_encoded": search,
                "is_all": (typeof type === "undefined"),
                "is_players": (type === "/players"),
                "is_championships": (type === "/championships")
            }
        });
});

router.add("^/players/(\\w+)", function(url, pid) {
    pid = parseInt(pid);
    load_and_compile("players.php?q="+pid,
        pathToTemplate+"/player.tpl",
        function(data) {
            return {
                "dob": date_to_str(data.dob)
            }
        });
});

router.add("^/add$", function() {
    load_and_compile("allmatchs.php", pathToTemplate+"/add.tpl");
});


// Start the router by binding it to the window hash change
// and the window loader
router.init();



Handlebars.registerHelper("if_eq", function(a, b, opts) {
    if(a == b) return opts.fn(this);
    else       return opts.inverse(this);
});
Handlebars.registerHelper("ifOR", function(a, b, opts) {
    if(a || b) return opts.fn(this);
    else       return opts.inverse(this);
});
Handlebars.registerHelper("sum", function(a, b) {
    return ""+(parseInt(a)+parseInt(b));
});
