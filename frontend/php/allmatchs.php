{
    "matchs": [
	<?php
	$seperator = "";
	$query = "SELECT `match`.ID, `match`.BESTOF FROM `match`;";
	$db = new PDO("mysql:dbname=snooker", "root", "");
	$rows = $db->query($query);
	foreach($rows as $row) {
		$ID = $row[0];
		
		$winID = "";
		$loseID = "";
		$winner = "";
		$loser = "";
		$winScore = 0;
		$loseScore = 0;
		$champ = "";
		
		$query = "SELECT Winner_id, Loser_id FROM friendly_match WHERE Match_id = $ID ;";
		$matches = $db->query($query);
		foreach($matches as $match) {
			$winID = $match[0];
			$loseID = $match[1];
			
			$query = "SELECT Name FROM player WHERE ID = $winID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$winner = $player[0];
			}
			
			$query = "SELECT Name FROM player WHERE ID = $loseID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$loser = $player[0];
			}
			
			$query = "SELECT COUNT(Frame_id) FROM frames WHERE Match_id = $ID and Winner_id = $winID ;";
			$frames = $db->query($query);
			foreach($frames as $frm) {
				$winScore = $frm[0];
			}
			
			$query = "SELECT COUNT(Frame_id) FROM frames WHERE Match_id = $ID and Winner_id = $loseID ;";
			$frames = $db->query($query);
			foreach($frames as $frm) {
				$loseScore = $frm[0];
			}
		}
		
		$query = "SELECT tournament_match.Pro_winner_id, tournament_match.Pro_loser_id, championship.Name FROM tournament_match JOIN tournament ON tournament.ID = tournament_match.TOURNAMENT_id JOIN championship ON championship.ID = tournament.CHAMPIONSHIP_ID WHERE Match_id = $ID ;";
		$matches = $db->query($query);
		foreach($matches as $match) {			
			$winID = $match[0];
			$loseID = $match[1];
			$champ = "- ".$match[2];
			
			$query = "SELECT Name FROM player WHERE ID = $winID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$winner = $player[0];
			}
			
			$query = "SELECT Name FROM player WHERE ID = $loseID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$loser = $player[0];
			}
			
			$query = "SELECT COUNT(Frame_id) FROM frames WHERE Match_id = $ID and Winner_id = $winID ;";
			$frames = $db->query($query);
			foreach($frames as $frm) {
				$winScore = $frm[0];
			}
			
			$query = "SELECT COUNT(Frame_id) FROM frames WHERE Match_id = $ID and Winner_id = $loseID ;";
			$frames = $db->query($query);
			foreach($frames as $frm) {
				$loseScore = $frm[0];
			}
		}
	?>
		<?=$seperator?>
		{
			"id": <?=$ID?>,
			"name": "<?=$winner?> <?=$winScore?> (<?=$row[1]?>) <?=$loseScore?> <?=$loser?> <?=$champ?>"
		}
	<?php
		$seperator = ",";
	}
	?>
    ],
    "players": [
	<?php
	$seperator = "";
	$query = "SELECT ID, Name FROM player;";
	$db = new PDO("mysql:dbname=snooker", "root", "");
	$rows = $db->query($query);
	foreach($rows as $row) {
	?>
		<?=$seperator?>
        {
        "id": <?=$row[0]?>,
        "name": "<?=$row[1]?>"
        }
	<?php
		$seperator = ",";
	}
	?>
    ]
}
