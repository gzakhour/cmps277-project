{
<?php
$seperator = "";
$ID = $_REQUEST['id'];

$query = "SELECT BESTOF, UNIX_TIMESTAMP(`match`.`DATE`) FROM `match` where ID = $ID;";
$db = new PDO("mysql:dbname=snooker", "root", "");
$rows = $db->query($query);
foreach($rows as $row) {
?>
	"bestof": <?=$row[0]?>,
	"date": <?=$row[1]?>,
		
	<?php
	$type = "";
	$name1 = "";
	$pic1 = "";
	$id1 = "";
	$name2 = "";
	$pic2 = "";
	$id2 = "";
	$ref = "";
	$champname = "";
	$champid = "";
	$dur = 0;
	
	$query = "SELECT * FROM friendly_match WHERE MATCH_ID = $ID ;";
	$friendlies = $db->query($query);
	foreach($friendlies as $frnd) {
		$type = "Friendly";
		$id1 = $frnd['Winner_id'];
		$id2 = $frnd['Loser_id'];
		$ref = "-";
		$champname = "";
	}
	
	$query = "SELECT tournament_match.Pro_winner_id, tournament_match.Pro_loser_id, referee.Name, championship.Name, tournament.ID, tournament.YEAR FROM tournament_match JOIN referee on tournament_match.Referee_id = referee.ID JOIN tournament ON tournament.ID = tournament_match.TOURNAMENT_id JOIN championship ON championship.ID = tournament.CHAMPIONSHIP_ID WHERE tournament_match.MATCH_ID = $ID ;";
	$pros = $db->query($query);
	foreach($pros as $pro) {
		$type = "Tournament";
		$id1 = $pro[0];
		$id2 = $pro[1];
		$ref = $pro[2];
		$champname = $pro[3]." - ".$pro[5];
		$champid = $pro[4];
	}
	
	$query = "SELECT player.Name, player_img.IMG FROM player JOIN player_img on player_img.PLAYER_ID = player.ID WHERE ID = $id1 LIMIT 1;";
	$players = $db->query($query);
	foreach($players as $player) {
		$name1 = $player[0];
		$pic1 = $player[1];
	}
	
	$query = "SELECT player.Name, player_img.IMG FROM player JOIN player_img on player_img.PLAYER_ID = player.ID WHERE ID = $id2 LIMIT 1;";
	$players = $db->query($query);
	foreach($players as $player) {
		$name2 = $player[0];
		$pic2 = $player[1];
	}
	?>

    "type": "<?=$type?>",
	
	"players": [
        {
            "name": "<?=$name1?>",
            "pic": "<?=$pic1?>",
            "id": <?=$id1?>
        },
        {
            "name": "<?=$name2?>",
            "pic": "<?=$pic2?>",
            "id": <?=$id2?>
        }
    ],
	
	"referee": "<?=$ref?>",
	
    "championship": {
        "name": "<?=$champname?>",
        "id": "<?=$champid?>"
    },
	
	"frames": [
	<?php
	$query = "SELECT Winner_id, Loser_id, Winner_score, Loser_score, `TIME` FROM frames where Match_id = $ID;";
	$frames = $db->query($query);
	foreach($frames as $frame) {		
		$win_ID = $frame[0];
		$los_ID = $frame[1];
		$side1 = $frame[2];
		$side2 = $frame[3];
		
		if($win_ID == $id2) {
			$tmp = $side1;
			$side1 = $side2;
			$side2 = $tmp;
		}
	?>
		<?=$seperator?>
        [ <?=$side1?> , <?=$side2?> ]
	<?php
		$dur += $frame[4];
		$seperator = ",";
	}
	?>
    ],
	
    "duration": <?=$dur?>
<?php
}
?>
}
