<?php
$seperator = "";
$query = "SELECT Name, UNIX_TIMESTAMP(DATEOFBIRTH) FROM player where ID = ".$_REQUEST['q'].";";
$db = new PDO("mysql:dbname=snooker", "root", "");
$rows = $db->query($query);
foreach($rows as $row) {
	$type = "Amateur";
	$potrate = "-";
    $centuries = "-";
    $halfcenturies = "-";
    $no147 = "-";
	
	$query = "SELECT * FROM pro_player WHERE PLAYER_ID = ".$_REQUEST['q'].";";
	$pros = $db->query($query);
	foreach($pros as $pro) { 
		$type = "Professional";
		$potrate = $pro["Pot_rate"];
		$centuries = $pro["NoCenturies"];
		$halfcenturies = $pro["NoHalfCenturies"];
		$no147 =$pro["No147"];
	}
	?>
	{
		"name": "<?=$row[0]?>",
		"dob":  <?=$row[1]?>,
		"type": "<?=$type?>",
		"potrate": "<?=$potrate?>",
		"centuries": "<?=$centuries?>",
		"half-centuries": "<?=$halfcenturies?>",
		"no147": "<?=$no147?>",
		"pics": [
		<?php
		$seperator = "";
		$query = "SELECT IMG FROM player_img WHERE PLAYER_ID = ".$_REQUEST['q'].";";
		$imgs = $db->query($query);
		foreach($imgs as $img) {
		?>
			<?=$seperator?> "<?=$img[0]?>"			
		<?php
			$seperator = ",";
		}
		?>
		],
		"cues": [
		<?php
		$seperator = "";
		$query = "SELECT * FROM cue_stick JOIN player_cue_stick ON CUE_STICK_Model_No = Model_No WHERE PLAYER_ID = ".$_REQUEST['q'].";";
		$cues = $db->query($query);
		foreach($cues as $cue) {
		?>
			<?=$seperator?> "<?=$cue['Name']?> (<?=$cue['Wood']?> , <?=$cue['Weight']?>g)"			
		<?php
			$seperator = ",";
		}
		?>
		],
		<?php
		$winsC = 0;
		$loseC = 0;
		
		$query = "SELECT COUNT(Match_id) FROM tournament_match WHERE Pro_winner_id = ".$_REQUEST['q'].";";
		$counts = $db->query($query);
		foreach($counts as $c) {
			$winsC += $c[0];
		}
		
		$query = "SELECT COUNT(Match_id) FROM  friendly_match WHERE Winner_id = ".$_REQUEST['q'].";";
		$counts = $db->query($query);
		foreach($counts as $c) {
			$winsC += $c[0];
		}
		
		$query = "SELECT COUNT(Match_id) FROM tournament_match WHERE Pro_loser_id = ".$_REQUEST['q'].";";
		$counts = $db->query($query);
		foreach($counts as $c) {
			$loseC += $c[0];
		}
		
		$query = "SELECT COUNT(Match_id) FROM  friendly_match WHERE Loser_id = ".$_REQUEST['q'].";";
		$counts = $db->query($query);
		foreach($counts as $c) {
			$loseC += $c[0];
		}
		?>
		"wins" : <?=$winsC?>,
		"loses" : <?=$loseC?> 
	}
	<?
}

?>