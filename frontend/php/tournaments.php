{
    "tournament": {
	<?php
	$ID = $_REQUEST['id'];
	$query = "SELECT tournament.`YEAR`, championship.Name, championship.ID FROM tournament JOIN championship ON championship.ID = tournament.CHAMPIONSHIP_ID WHERE tournament.ID = $ID ;";
	$db = new PDO("mysql:dbname=snooker", "root", "");
	$rows = $db->query($query);
	foreach($rows as $row) {
	?>
        "year": <?=$row[0]?>,
        "championship": {
            "id": <?=$row[2]?>,
            "name": "<?=$row[1]?>"
        },
        "matchs": [
		<?php
		$seperator = "";
		$query = "SELECT tournament_match.Pro_winner_id, tournament_match.Pro_loser_id, `match`.ID, `match`.BESTOF FROM tournament_match JOIN `match` ON `match`.ID = tournament_match.Match_id WHERE tournament_match.TOURNAMENT_id = $ID ;";
		$matchs = $db->query($query);
		foreach($matchs as $match) {
			$matchID = $match[2];
		?>
            <?=$seperator?>
			{
                "bestof": <?=$match[3]?>,
                "id": <?=$matchID?>,
				<?php
				$winID = $match[0];
				$loseID = $match[1];
				
				$winner = "";
				$loser = "";
				
				$query = "SELECT Name FROM player WHERE ID = $winID ;";
				$players = $db->query($query);
				foreach($players as $play) {
					$winner = $play[0];
				}
				
				$query = "SELECT Name FROM player WHERE ID = $loseID ;";
				$players = $db->query($query);
				foreach($players as $play) {
					$loser = $play[0];
				}
				?>
				"players": [
                    "<?=$winner?>",
                    "<?=$loser?>"
                ],
				<?php
				$winCount = 0;
				$losCount = 0;
				
				$query = "SELECT COUNT(Frame_id) FROM frames WHERE Match_id = $matchID and Winner_id = $winID ;";
				$counts = $db->query($query);
				foreach($counts as $c) {
					$winCount = $c[0];
				}
				
				$query = "SELECT COUNT(Frame_id) FROM frames WHERE Match_id = $matchID and Winner_id = $loseID ;";
				$counts = $db->query($query);
				foreach($counts as $c) {
					$losCount = $c[0];
				}
				?>				
                "score": [
                    <?=$winCount?>,
                    <?=$losCount?>
                ]
            }
		<?php
			$seperator = ",";
		}
		?>
        ]
	<?
	}
	?>	
    }
}
