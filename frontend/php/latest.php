{
    "matchs": [
	<?php
	$seperator = "";
	$query = "SELECT `match`.ID, `match`.BESTOF FROM `match` ORDER BY (ID) desc LIMIT 5;";
	$db = new PDO("mysql:dbname=snooker", "root", "");
	$rows = $db->query($query);
	foreach($rows as $row) {
		$ID = $row[0];
		
		$winID = "";
		$loseID = "";
		$winner = "";
		$loser = "";
		$type = "";
		
		$query = "SELECT Winner_id, Loser_id FROM friendly_match WHERE Match_id = $ID ;";
		$matches = $db->query($query);
		foreach($matches as $match) {
			$winID = $match[0];
			$loseID = $match[1];
			$type = "Friendly";
			
			$query = "SELECT Name FROM player WHERE ID = $winID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$winner = $player[0];
			}
			
			$query = "SELECT Name FROM player WHERE ID = $loseID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$loser = $player[0];
			}
		}
		
		$query = "SELECT tournament_match.Pro_winner_id, tournament_match.Pro_loser_id, championship.Name FROM tournament_match JOIN tournament ON tournament.ID = tournament_match.TOURNAMENT_id JOIN championship ON championship.ID = tournament.CHAMPIONSHIP_ID WHERE Match_id = $ID ;";
		$matches = $db->query($query);
		foreach($matches as $match) {
			$winID = $match[0];
			$loseID = $match[1];
			$type = "Tournament";
			
			$query = "SELECT Name FROM player WHERE ID = $winID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$winner = $player[0];
			}
			
			$query = "SELECT Name FROM player WHERE ID = $loseID ;";
			$players = $db->query($query);
			foreach($players as $player) {
				$loser = $player[0];
			}
		}
	?>
		<?=$seperator?>
		{
            "id": <?=$ID?>,
            "player1": {
                "id": <?=$winID?>,
                "name": "<?=$winner?>"
            },
            "player2": {
                "id": <?=$loseID?>,
                "name": "<?=$loser?>"
            },
            "type": "<?=$type?>"
        }
	<?php
		$seperator = ",";
	}
	?>
    ],
    "tournaments": [
	<?php
	$seperator = "";	
	$query = "SELECT tournament.ID, tournament.`YEAR`, championship.Name FROM tournament JOIN championship ON championship.ID = tournament.CHAMPIONSHIP_ID ORDER BY(tournament.ID) desc LIMIT 5;";
	$rows = $db->query($query);
	foreach($rows as $row) {
	?>
		<?=$seperator?>
        {
            "id": <?=$row[0]?>,
            "name": "<?=$row[2]?>",
            "year": <?=$row[1]?>
        }
	<?php
		$seperator = ",";
	}
	?>
    ]
}
