# SnookerDB
This website is the project for CMPS 277: Database Systems. The website is about
listing all the snooker players and the all the snooker tournaments.

## Members
The group members are:

* George Zakhour
* Kinan Bab
* Ali Cherry

## Dependencies
The Makefile runs a local webserver using python to test everything on. The
local web server is `SimpleHTTPServer`. To download it please either download it
through pip (Python's package manager) `pip install SimpleHTTPServer` or through
your package manager.

## Legal
This program is licensed under MIT License
